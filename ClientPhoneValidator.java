package validators;

import model.Client;

public class ClientPhoneValidator implements Validator<Client> {
	//telefonul trebuie sa aiba 10 cifre si sa inceapa cu 07
	public void validate(Client t) {
		if (t.getPhone().length() != 10 || (t.getPhone().charAt(0)!='0' && t.getPhone().charAt(1)!='7')) {
			throw new IllegalArgumentException("The client phone number is not valid!");
		}
			
		

	}
}
