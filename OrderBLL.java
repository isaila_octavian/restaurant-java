package bll;



import java.util.ArrayList;
import java.util.List;

import dao.OrderDAO;
import model.Order;
import validators.AmountValidator;
import validators.Validator;

public class OrderBLL {
	private OrderDAO orderDAO;
	private List<Validator<Order>> validators; 
	public OrderBLL() {
		orderDAO = new OrderDAO();
		validators=new ArrayList<Validator<Order>>();
		validators.add(new AmountValidator());
	}
	public void insertOrder(Order o) {
		for (Validator<Order> v : validators) {
			v.validate(o);
		}
		orderDAO.insert(o);
	}
	public void edit(int id,Order c) {
		for (Validator<Order> v : validators) {
			v.validate(c);
		}
		orderDAO.update(id,c);
	}
	public void delete(int id) {
		orderDAO.delete(id);
	}
	public List<Order> list() {
		return orderDAO.findAll();
	}
}
