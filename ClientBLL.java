package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import model.Client;
import validators.ClientNameValidator;
import validators.ClientPhoneValidator;
import validators.Validator;
import dao.CustomerDAO;


public class ClientBLL {
	private CustomerDAO customerDAO;
	private List<Validator<Client>> validators; 
	
	public ClientBLL() {
		customerDAO = new CustomerDAO();
		validators=new ArrayList<Validator<Client>>();
		validators.add(new ClientPhoneValidator());
		validators.add(new ClientNameValidator());
	}
	public Client findClientById(int idcustomer) {
		Client c=customerDAO.findById(idcustomer);
		if (c == null) {
			throw new NoSuchElementException("The client with id  = " + idcustomer + " was not found!");
		}
		return c;
	}
	public void insertClient(Client c) {
		for (Validator<Client> v : validators) {
			v.validate(c);
		}
		customerDAO.insert(c);
	}
	public void edit(int id,Client c) {
		for (Validator<Client> v : validators) {
			v.validate(c);
		}
		customerDAO.update(id,c);
	}
	public void delete(int id) {
		customerDAO.delete(id);
	}
	public List<Client> list() {
		return customerDAO.findAll();
	}
}
