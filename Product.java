package model;

public class Product {
	private int idproduct;
	private String name;
	private float price;
	private float rating;
	private String category;
	private int amount;
	public Product() {
		
	}
	public Product(int idproduct, String name, float price, float rating, String category, int amount) {
		super();
		this.idproduct=idproduct;
		this.name=name;
		this.price=price;
		this.rating=rating;
		this.category=category;
		this.amount=amount;
	}
	public int getIdproduct() {
		return idproduct;
	}

	public void setIdproduct(int idproduct) {
		this.idproduct = idproduct;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "Product [id=" + idproduct + ", name=" + name + ", price=" + price +  ", rating=" + rating
				+ ", category=" + category + "]";
	}
}
