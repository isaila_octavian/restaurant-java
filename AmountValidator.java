package validators;

import model.Order;


public class AmountValidator  implements Validator<Order>{
	public void validate(Order t) {
		if (t.getAmount()<0) {
			throw new IllegalArgumentException("The amount can not be negative!");
		}
	}
}
