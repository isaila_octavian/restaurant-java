package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import model.Client;
import model.Order;
import model.Product;

public class Controller {
	private View view;
	private Listener btnListener;

	private ClientBLL customerBLL = new ClientBLL();
	private ProductBLL productBLL = new ProductBLL();
	private OrderBLL orderBLL=new OrderBLL();
	
	public Controller(View view) {
		this.view = view;
		btnListener = new Listener();
		view.addBtnListener(btnListener);
	}
	private <T> JTable createTable(List<T> object) {
		JTable table = null;
		int numberOfColumns = object.get(0).getClass().getDeclaredFields().length;
		String columns[] = new String[numberOfColumns];
		int i=0;
		for (Field field : object.get(0).getClass().getDeclaredFields()) {
			columns[i] = field.getName();
			i++;
		
		}
		String rows[][] = new String[object.size()][numberOfColumns];
		int row = 0;
		for (Object obj : object) {
			int col = 0;
			for (Field field : obj.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				Object value;
				try {
					value = field.get(obj);
					rows[row][col] = value.toString();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				col++;
			}
			row++;
		}
		table = new JTable(rows,columns);
		return table;
	}
	class Listener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (e.getActionCommand()=="insertClient") 
		
				view.setNewInsertClientView(btnListener);

			else 
				if(e.getActionCommand()=="insertProduct")  
				view.setNewInsertProductView(btnListener);
			else if(e.getActionCommand()=="listClients" )	
				view.setTable(createTable(customerBLL.list()));
			else if(e.getActionCommand()=="listProducts" )
				view.setTable(createTable(productBLL.list()));
			else if(e.getActionCommand()=="deleteClient" )
				{int id1=Integer.parseInt(view.getTable().getValueAt(view.getTable().getSelectedRow(), 0).toString());
				customerBLL.delete(id1);
				view.setTable(createTable(customerBLL.list()));
				view.showMessage("Client deleted");}
			else if(e.getActionCommand()=="deleteProduct" )
			{
				int id2=Integer.parseInt(view.getTable().getValueAt(view.getTable().getSelectedRow(), 0).toString());
				productBLL.delete(id2);
				view.setTable(createTable(productBLL.list()));
				view.showMessage("Product deleted");}
			else if(e.getActionCommand()=="insertClient2") {
				int idd=Integer.parseInt(view.v1.textId.getText());
				String namee=view.v1.textName.getText();
				String phonee=view.v1.textPhone.getText();
				Client c=new Client(idd,namee,phonee);
				customerBLL.insertClient(c);
				view.v1.dispose();
				view.showMessage("Client inserted!");}
			else if(e.getActionCommand()=="insertProduct1") {
				int id=Integer.parseInt(view.v2.textId.getText());
				String name=view.v2.textName.getText();
				float price=Float.parseFloat(view.v2.textPrice.getText());
				float rating=Float.parseFloat(view.v2.textRating.getText());
				String category=view.v2.textCategory.getText();
				int amount=Integer.parseInt(view.v2.textAmount.getText());
				Product p=new Product(id,name,price,rating,category,amount); 
				productBLL.insertProduct(p);
				view.v2.dispose();
				view.showMessage("Product inserted!");}
			else if(e.getActionCommand()=="searchClient") {
				List<Object> list=new ArrayList<Object>();
				int idS=Integer.parseInt(view.id1.getText());
				Object ce=customerBLL.findClientById(idS);
				list.add(ce);
				view.setTable(createTable(list)); 
			}
			else if(e.getActionCommand()=="searchProduct") {
				List<Object> list2=new ArrayList<Object>();
				int idSP=Integer.parseInt(view.id2.getText());
				Object pe=productBLL.findProductById(idSP);
				list2.add(pe);
				view.setTable(createTable(list2)); 
			}
			else if(e.getActionCommand()=="updateClient") {
				int idU=Integer.parseInt(view.getTable().getValueAt(view.getTable().getSelectedRow(), 0).toString());
				String nameU=view.getTable().getValueAt(view.getTable().getSelectedRow(), 1).toString();
				String phoneU=view.getTable().getValueAt(view.getTable().getSelectedRow(), 2).toString();
				Client cu=new Client(idU,nameU,phoneU);
				customerBLL.edit(idU, cu);
				view.setTable(createTable(customerBLL.list()));
				view.showMessage("Client updated");
			}
			else if(e.getActionCommand()=="updateProducts") {
				int idUp=Integer.parseInt(view.getTable().getValueAt(view.getTable().getSelectedRow(), 0).toString());
				String nameUp=view.getTable().getValueAt(view.getTable().getSelectedRow(), 1).toString();
				float priceu=Float.parseFloat(view.getTable().getValueAt(view.getTable().getSelectedRow(), 2).toString());
				float ratingu=Float.parseFloat(view.getTable().getValueAt(view.getTable().getSelectedRow(), 3).toString());
				String categoryUp=view.getTable().getValueAt(view.getTable().getSelectedRow(), 4).toString();
				int amountu=Integer.parseInt(view.getTable().getValueAt(view.getTable().getSelectedRow(), 5).toString());
				Product pp=new Product(idUp,nameUp,priceu,ratingu,categoryUp,amountu);
				productBLL.edit(idUp, pp);
				view.setTable(createTable(productBLL.list()));
				view.showMessage("Product updated");
			}
			else if(e.getActionCommand()=="placeOrder") {
				view.setNewOrderView(createTable(customerBLL.list()), createTable(productBLL.list()),btnListener);
			}
			else if(e.getActionCommand()=="confirmorder") {
				int selectedRow = view.v3.getClientsTable().getSelectedRow();
				int id = Integer.parseInt(view.v3.getClientsTable().getValueAt(selectedRow, 0).toString());
				String name = view.v3.getClientsTable().getValueAt(selectedRow, 1).toString();
				String phone= view.v3.getClientsTable().getValueAt(selectedRow, 2).toString();
				Client selectedClient =new Client(id,name,phone);
				int selectedRow1 = view.v3.getProductsTable().getSelectedRow();
				int id1 = Integer.parseInt(view.v3.getProductsTable().getValueAt(selectedRow1, 0).toString());
				String name1 = view.v3.getProductsTable().getValueAt(selectedRow1, 1).toString();
				float price = Float.parseFloat(view.v3.getProductsTable().getValueAt(selectedRow1, 2).toString());
				float rating = Float.parseFloat(view.v3.getProductsTable().getValueAt(selectedRow1, 3).toString());
				String category = view.v3.getProductsTable().getValueAt(selectedRow1, 4).toString();
				int amount = Integer.parseInt(view.v3.getProductsTable().getValueAt(selectedRow1, 5).toString());
				Product selectedProduct =new Product(id1,name1,price,rating,category,amount);
				int amountt=Integer.parseInt(view.v3.textAmount.getText());
				if(amountt<=selectedProduct.getAmount()) {
				Order o=new Order(amountt,selectedClient.getIdcustomer(),selectedProduct.getIdproduct());
				orderBLL.insertOrder(o);
				view.showMessage("Product bought!");
				selectedProduct.setAmount(selectedProduct.getAmount() - amountt);
				productBLL.edit(selectedProduct.getIdproduct(), selectedProduct);
				view.setTable(createTable(productBLL.list()));
				view.v3.setProductTable(createTable(productBLL.list()));
				}
				else {
					view.showMessage("Understock!");
				}
			}

			}		
		}
	}

