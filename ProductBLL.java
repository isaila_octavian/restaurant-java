package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import dao.ProductDAO;
import model.Product;
import validators.PriceValidator;
import validators.RatingValidator;
import validators.Validator;

public class ProductBLL {
	private ProductDAO productDAO;
	private List<Validator<Product>> validators; 
	public ProductBLL() {
		productDAO = new ProductDAO();
		validators=new ArrayList<Validator<Product>>();
		validators.add(new PriceValidator());
		validators.add(new RatingValidator());
	}
	public Product findProductById(int idproduct) {
		Product c=productDAO.findById(idproduct);
		if (c == null) {
			throw new NoSuchElementException("The product with id  = " + idproduct + " was not found!");
		}
		return c;
	}
	public void insertProduct(Product c) {
		for (Validator<Product> v : validators) {
			v.validate(c);
		}
		productDAO.insert(c);
	}
	public void edit(int id,Product c) {
		for (Validator<Product> v : validators) {
			v.validate(c);
		}
		productDAO.update(id,c);
	}
	public void delete(int id) {
		productDAO.delete(id);
	}
	public List<Product> list() {
		return productDAO.findAll();
	}
}
