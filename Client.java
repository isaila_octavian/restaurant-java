package model;

public class Client {
	private int idcustomer;
	private String name;
	private String phone;
	public Client() {
		
	}
	public Client(int idcustomer, String name, String phone) {
		super();
		this.idcustomer=idcustomer;
		this.name=name;
		this.phone=phone;
	}
	public int getIdcustomer() {
		return idcustomer;
	}

	public void setIdcustomer(int idcustomer) {
		this.idcustomer = idcustomer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "Clientul: [idcustomer=" + idcustomer + ", name=" + name + ", phone=" + phone + "]";
	}
	
	
}
