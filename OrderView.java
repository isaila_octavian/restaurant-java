package presentation;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class OrderView extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel pane;
	private JTable clientss=new JTable();
	private JTable products=new JTable();;
	private JButton confirmOrder=new JButton("Confirm Order!");
	 JLabel amount=new JLabel("Amount: ");
	 JTextField textAmount=new JTextField();
	private JScrollPane scrollPane1=new JScrollPane();
	private JScrollPane scrollPane2=new JScrollPane();
	public OrderView(JTable clientss, JTable products) {
		this.clientss=clientss;
		this.products=products;
		setTitle("Make the order");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 730, 450);
		pane = new JPanel();
		pane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pane);
		pane.setLayout(null);
		
		scrollPane1.setBounds(20, 20, 300, 250);
		scrollPane1.setViewportView(clientss);
		pane.add(scrollPane1);	
		scrollPane2.setBounds(370, 20, 300, 250);
		scrollPane2.setViewportView(products);
		pane.add(scrollPane2);	
		amount.setBounds(200, 300, 150, 30);
		pane.add(amount);
		textAmount.setBounds(300, 300, 150, 30);
		pane.add(textAmount);
		confirmOrder.setBounds(260, 350, 150, 30);
		pane.add(confirmOrder);
		confirmOrder.setActionCommand("confirmorder");
		this.setVisible(true);
	}
	public void addBtnListener(ActionListener listener) {
		confirmOrder.addActionListener(listener);
	}
	public JTable getClientsTable() {
		return clientss;
	}
	public JTable getProductsTable() {
		return products;
	}
	public void setProductTable(JTable table) {
		this.products = table;
		scrollPane2.setViewportView(products);
		repaint();
		revalidate();
	}
}
