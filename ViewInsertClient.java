package presentation;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class ViewInsertClient extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private  JPanel pane=new JPanel();
	JTextField textId=new JTextField();
	JTextField textName=new JTextField();
	JTextField textPhone=new JTextField();
	JLabel name=new JLabel("Name: ");
	JLabel id=new JLabel("Id: ");
	JLabel phone=new JLabel("Phone: ");
	private JButton btnInsert=new JButton("Insert client");
	public ViewInsertClient() {
		setTitle("Insert Client");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 400);
		pane = new JPanel();
		pane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pane);
		pane.setLayout(null);
		id.setBounds(50, 30, 150, 50);
		pane.add(id);
		textId.setBounds(100, 40, 150, 30);
		pane.add(textId);
		name.setBounds(50, 100, 150, 50);
		pane.add(name);
		textName.setBounds(100, 110, 150, 30);
		pane.add(textName);
		phone.setBounds(50, 170, 150, 50);
		pane.add(phone);
		textPhone.setBounds(100, 180, 150, 30);
		pane.add(textPhone);
		btnInsert.setBounds(130, 260, 150, 30);
		pane.add(btnInsert);
		btnInsert.setActionCommand("insertClient2");
		this.setVisible(true);
	}
	public JButton getBtnInsert() {
		return btnInsert;
	}
	
	public void addBtnListener(ActionListener listener) {
		btnInsert.addActionListener(listener);
	}
}
