package validators;

import model.Client;

public class ClientNameValidator implements Validator<Client>{
	//daca numele nu contine caractere speciale, atunci e un nume valid
	public void validate(Client t) {
		String nume=t.getName().toLowerCase();
		for (int i=0; i<nume.length(); i++) {
			if ((nume.charAt(i)<'a' || nume.charAt(i)>'z') && nume.charAt(i)!=' ')
				throw new IllegalArgumentException("The client name is not valid!");
		}
		

	}
}
