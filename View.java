package presentation;


import java.awt.event.ActionListener;


import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import bll.ClientBLL;


public class View extends JComponent{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected static JFrame frame=new JFrame();
	ViewInsertClient v1;
	ViewInsertProduct v2;
	ClientBLL customerBLL=new ClientBLL();
	OrderView v3;
	 JPanel pan=new JPanel();
	 JButton insertC=new JButton("Insert client");
	 JButton insertP=new JButton("Insert product");
	 JButton deleteC=new JButton("Delete client");
	 JButton deleteP=new JButton("Delete product");
	 JButton updateC=new JButton("Update client");
	 JButton updateP=new JButton("Update product");
	 JButton listC=new JButton("List clients");
	 JButton listP=new JButton("List products");
	 JButton listO=new JButton("List orders");
	 JButton place=new JButton("Place Order");
	 JButton searchC=new JButton("Search customer");
	 JButton searchP=new JButton("Search product");
	 JLabel searchCid= new JLabel ("ID: ") ;
	 JLabel searchPid= new JLabel ("ID: ") ;
	 JTextField id1 = new JTextField () ;
	 JTextField id2 = new JTextField () ;
	 private JTable table;
	 private JScrollPane scrollPane=new JScrollPane();
	 public View() {
		 frame=new JFrame("Warehouse management");
			frame.setBounds(100, 100, 500, 600);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			pan.setLayout(null);
			frame.setContentPane(pan);
			frame.setResizable(false);
			insertC.setBounds(50, 50, 150, 30);
			frame.getContentPane().add(insertC);
			insertC.setActionCommand("insertClient");
			deleteC.setBounds(50, 90, 150, 30);
			frame.getContentPane().add(deleteC);
			deleteC.setActionCommand("deleteClient");
			listC.setBounds(50, 130, 150, 30);
			frame.getContentPane().add(listC);
			listC.setActionCommand("listClients");
			updateC.setBounds(50, 170, 150, 30);
			frame.getContentPane().add(updateC);
			updateC.setActionCommand("updateClient");
			insertP.setBounds(300, 50, 150, 30);
			frame.getContentPane().add(insertP);
			insertP.setActionCommand("insertProduct");
			deleteP.setBounds(300, 90, 150, 30);
			frame.getContentPane().add(deleteP);
			deleteP.setActionCommand("deleteProduct");
			listP.setBounds(300, 130, 150, 30);
			frame.getContentPane().add(listP);
			listP.setActionCommand("listProducts");
			updateP.setBounds(300, 170, 150, 30);
			frame.getContentPane().add(updateP);
			updateP.setActionCommand("updateProducts");
			place.setBounds(170, 210, 150, 30);
			frame.getContentPane().add(place);
			place.setActionCommand("placeOrder");
			scrollPane.setBounds(10, 250, 470, 140);
			frame.add(scrollPane);
			searchC.setBounds(80, 410, 150, 30);
			frame.getContentPane().add(searchC);
			searchC.setActionCommand("searchClient");
			searchP.setBounds(80, 470, 150, 30);
			frame.getContentPane().add(searchP);
			searchP.setActionCommand("searchProduct");
			searchCid.setBounds(250, 410, 150, 30);
			frame.getContentPane().add(searchCid);
			searchPid.setBounds(250, 470, 150, 30);
			frame.getContentPane().add(searchPid);
			id1.setBounds(280, 410, 150, 30);
			frame.getContentPane().add(id1);
			id2.setBounds(280, 470, 150, 30);
			frame.getContentPane().add(id2);
			frame.setVisible(true);
		 }
	 public void addBtnListener(ActionListener listener) {
			insertC.addActionListener(listener);
			insertP.addActionListener(listener);
			deleteC.addActionListener(listener);
			deleteP.addActionListener(listener);
			updateC.addActionListener(listener);
			updateP.addActionListener(listener);
			listC.addActionListener(listener);
			listP.addActionListener(listener);
			place.addActionListener(listener);
			searchC.addActionListener(listener);
			searchP.addActionListener(listener);
		}
	 public void setTable(JTable newTable) {
			this.table = newTable;
			scrollPane.setViewportView(table);
			repaint();
			revalidate();
		}
	 public JTable getTable() {
			return table;
		}
	 public String getIDC() {
			return id1.getText();
		}
	 public String getIDP() {
			return id2.getText();
		}
	 public void setNewInsertClientView(ActionListener listener) {
			 v1 = new ViewInsertClient();
			v1.addBtnListener(listener);
		}
	 public void setNewInsertProductView(ActionListener listener) {
			 v2 = new ViewInsertProduct();
			v2.addBtnListener(listener);
		}
	 public void setNewOrderView(JTable customers, JTable products, ActionListener listener) {
			v3 = new OrderView(customers,products);
			v3.addBtnListener(listener); 
		}
	 public void showMessage(String message) {
			JOptionPane.showMessageDialog(this, message);
		}
	 }