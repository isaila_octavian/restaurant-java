package validators;

import model.Product;
import validators.Validator;

public class RatingValidator implements Validator<Product> {
	public void validate(Product t) {
		if (t.getRating()<=0 && t.getRating()>10)
			throw new IllegalArgumentException("The rating must be in the interval [1;10]");
	}
}
