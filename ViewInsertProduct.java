package presentation;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class ViewInsertProduct extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private  JPanel pane=new JPanel();
	JTextField textId=new JTextField();
	JTextField textName=new JTextField();
	JTextField textPrice=new JTextField();
	JTextField textRating=new JTextField();
	JTextField textCategory=new JTextField();
	JTextField textAmount=new JTextField();
	JLabel name=new JLabel("Name: ");
	JLabel id=new JLabel("Id: ");
	JLabel price=new JLabel("Price: ");
	JLabel rating=new JLabel("Rating: ");
	JLabel category=new JLabel("Category: ");
	JLabel amount=new JLabel("Amount: ");
	private JButton btnInsert=new JButton("Insert product");
	public ViewInsertProduct() {
		setTitle("Insert Product");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 480, 540);
		pane = new JPanel();
		pane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pane);
		pane.setLayout(null);
		id.setBounds(50, 30, 150, 50);
		pane.add(id);
		textId.setBounds(100, 40, 150, 30);
		pane.add(textId);
		name.setBounds(50, 100, 150, 50);
		pane.add(name);
		textName.setBounds(100, 110, 150, 30);
		pane.add(textName);
		price.setBounds(50, 170, 150, 50);
		pane.add(price);
		textPrice.setBounds(100, 180, 150, 30);
		pane.add(textPrice);
		rating.setBounds(50, 240, 150, 50);
		pane.add(rating);
		textRating.setBounds(100, 250, 150, 30);
		pane.add(textRating);
		category.setBounds(50, 310, 150, 50);
		pane.add(category);
		textCategory.setBounds(110, 320, 150, 30);
		pane.add(textCategory);
		amount.setBounds(50, 380, 150, 50);
		pane.add(amount);
		textAmount.setBounds(110, 390, 150, 30);
		pane.add(textAmount);
		btnInsert.setBounds(140, 430, 150, 30);
		btnInsert.setActionCommand("insertProduct1");
		pane.add(btnInsert);
		this.setVisible(true);
	}
	public JButton getBtnInsert() {
		return btnInsert;
	}
	
	public void addBtnListener(ActionListener listener) {
		btnInsert.addActionListener(listener);
	}
}
